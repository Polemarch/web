function initSlider(name, firstTimeout){
	var container, images = [];
	const imageWidth = 600;

	var moving = false;
	var counter = 0;
	var uniqueInside = 0;

	var containerName;
	var logoMoveTimeout;

	containerName = name;
	container = document.getElementById(containerName);
	initImages(["image/ski1.jpg","image/ski2.jpg","image/ski3.jpg"]);

	loopImage();
	window.onkeydown = onKeyDown;

	function loopImage(){
		logoMoveTimeout = setTimeout(function(){
			clearTimeout(logoMoveTimeout);
			logoMoveTimeout = 0;
			move('left');
			loopImage();
		}, firstTimeout)
	}

	function onKeyDown(event){
		switch (event.keyCode){
			case 39:
				move('left');
				break;
			case 37:
				move('right');
				break;
		}
	}

	function move(moveSide = 'right'){
		if (moving) return;
		moving = true;

		var pos = (moveSide == 'right') ? -imageWidth : imageWidth;
		var moveTo = - pos;
		var nextPos = getCounterPos(moveSide);
		var image = images[nextPos];

		container.style.left = "0px";
		images[counter].style.left = "0px"
		image.style.left = pos + 'px';
		container.appendChild(image);
		$('#'+containerName)
		.animate({left:moveTo + 'px'}, {duration:800, complete:function(){
			moveComplete(nextPos);
		}});
	}

	function getCounterPos(side){
		var result = (side == 'right') ? counter + 1 : counter - 1;
		if (result < 0) result = images.length - 1;
		if (result > images.length - 1) result = 0;
		return result;
	}

	function moveComplete(place){
		moving = false;
		container.removeChild(images[counter]);
		counter = place;
	}

	function initImages(names){
		var image;
		for (var i = 0; i < names.length; i++) {
			image = document.createElement("img");
			image.src = names[i];
			if (image == null) continue;
			image.style.position = 'absolute';
			image.width = imageWidth;
			image.style.top = '0px';
			images.push(image);
		}
		container.appendChild(images[0]);
		console.log(images);
	}
}

